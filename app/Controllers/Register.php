<?php

namespace App\Controllers;

class Register extends BaseController
{
	public function index()
	{
		return view('pages/register');
	}

	public function save()
	{
		$session = session();
		$body = array(
			'firstName' => $this->request->getVar('name1'),
			'lastName' => $this->request->getVar('name2'),
			'email' => $this->request->getVar('email'),
			'password' => $this->request->getVar('password')
		);

		$client = \Config\Services::curlrequest();
		$response = $client->request('post', 'http://localhost:8080/api/users/register', ['json' => $body]);
		
		$obj = json_decode($response->getBody(), true);
		$session->set($obj);
		return redirect()->to(base_url().'/dashboard');
	}
}
