<?php

namespace App\Controllers;

class Login extends BaseController
{
	public function index()
	{
		$jwt = session('token');
		if($jwt != null){
			return redirect()->to(base_url().'/dashboard');
		} else {
			return view('pages/login');
		}
	}
	public function save(){
		$session = session();
		$body = array(
			'email' => $this->request->getVar('email'),
			'password' => $this->request->getVar('password')
		);
		$client = \Config\Services::curlrequest();
		$response = $client->request('post', 'http://localhost:8080/api/users/login', ['json' => $body]);
		$obj = json_decode($response->getBody(), true);
		$session->set($obj);
		return redirect()->to(base_url().'/dashboard');
	}

	public function logout(){
		$session = session();
		$session->destroy();
		return redirect()->to(base_url().'/login');
	}
}
