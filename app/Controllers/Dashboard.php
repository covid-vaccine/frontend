<?php

namespace App\Controllers;

class Dashboard extends BaseController
{
	public function index()
	{
		$jwt = session('token');
		if($jwt != null) {
			$client = \Config\Services::curlrequest();
			$response = $client->request('get', 'http://localhost:8080/api/registrant/user', [
        'headers' => [
                'Authorization' => 'Bearer '.$jwt
        ]
			]);
			$data['user'] = json_decode($response->getBody(), true);
			//var_dump($data);
			return view('pages/dashboard', $data);
		} else {
			return redirect()->to(base_url());
		}
	}
}
