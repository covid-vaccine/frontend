<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <!-- start linking  -->
  <link href="https://fonts.googleapis.com/css?family=Nunito:300,400,600,700,800,900" rel="stylesheet">
  <link href="https://use.fontawesome.com/releases/v5.0.4/css/all.css" rel="stylesheet">
  <link rel="stylesheet" href="assets3/css/bootstrap.min.css">
  <link rel="stylesheet" href="assets3/css/app.css">
  <!-- icon -->
  <link rel="icon" href="assets3/img/log.png">
  <!-- end linking -->
  <title>CeTas - dashboard</title>
</head>
<body>
<!-- start admin -->
<section id="admin">
  <!-- start sidebar -->
  <div class="sidebar">
    <!-- start with head -->
    <div class="head">
      <div class="logo">
        <img src="assets3/img/logo-admin.png" alt="">
      </div>
      <a href="/vaccine_registration" class="btn btn-danger">Registrasi Vaksin</a>
    </div>
    <!-- end with head -->
    <!-- start the list -->
    <div id="list">
      <ul class="nav flex-column">
        <li class="nav-item"><a href="/dashboard" class="nav-link active" ><i class="fa fa-adjust"></i>Dashboard</a></li>
        <!-- end user interface submenu -->
      </ul>
    </div>
    <!-- end the list -->
  </div>
  <!-- end sidebar -->

  <?= $this->renderSection('content'); ?>

</section>
<!-- end admin -->
<!-- start screpting -->
<script src="assets3/js/jquery.min.js"></script>
<script src="assets3/js/tether.min.js"></script>
<script src="assets3/js/bootstrap.min.js"></script>
<script src="assets3/js/highcharts.js"></script>
<script src="assets3/js/chart.js"></script>
<script src="assets3/js/app.js"></script>
<!-- end screpting -->
</body>
</html>
