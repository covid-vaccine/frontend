<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title> CeTas || Cegah dan Atasi || Registrasi dan reservasi Covid-19</title>
    <link rel="apple-touch-icon" sizes="180x180" href="assets/images/favicons/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="assets/images/favicons/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="assets/images/favicons/favicon-16x16.png">
    <link rel="manifest" href="assets/images/favicons/site.webmanifest">

    <!-- plugin scripts -->

    <link href="https://fonts.googleapis.com/css?family=Amatic+SC:400,700%7CBarlow:300,400,500,600,700,800,900%7CPT+Sans:400,700&display=swap" rel="stylesheet">


    <link rel="stylesheet" href="assets/css/animate.min.css">
    <link rel="stylesheet" href="assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="assets/css/owl.carousel.min.css">
    <link rel="stylesheet" href="assets/css/owl.theme.default.min.css">
    <link rel="stylesheet" href="assets/css/magnific-popup.css">
    <link rel="stylesheet" href="assets/css/fontawesome-all.min.css">
    <link rel="stylesheet" href="assets/css/bootstrap-select.min.css">
    <link rel="stylesheet" href="assets/css/jquery.mCustomScrollbar.min.css">
    <link rel="stylesheet" href="assets/css/bootstrap-datepicker.min.css">
    <link rel="stylesheet" href="assets/css/vimns-icons.css">



    <!-- template styles -->
    <link rel="stylesheet" href="assets/css/main.css">
    <link rel="stylesheet" href="assets/css/responsive.css">







</head>

<body>
    <div class="preloader">
        <img src="assets/images/loader.png" class="preloader__image" alt="">
    </div><!-- /.preloader -->
    <div class="page-wrapper">

        <header class="site-header-one">
            <nav class="main-nav__one stricky">
                <div class="container-fluid">
                    <div class="main-nav__logo-box">
                        <a href="index.html">
                            CeTas
                        </a>
                        <a href="#" class="side-menu__toggler"><i class="fa fa-bars"></i></a>
                    </div><!-- /.main-nav__logo-box -->
                    <div class="main-nav__main-navigation">
                        <ul class=" main-nav__navigation-box">
                            <li>
                                <a href="/">Beranda</a>
                            </li>
                            <li>
                                <a href="/about">Tentang</a>
                            </li>
                            <li>
                                <a href="/prevention">Pencegahan</a>
                            </li>
                            <li>
                                <a href="/faqs">Pertanyaan</a>
                            </li>
                            <li>
                                <a href="/vaccine_registration">Registrasi Vaksinasi</a>
                            </li>
                        </ul>
                    </div><!-- /.main-nav__main-navigation -->
                    
                    <div class="main-nav__right">
                        <a href="login" class="main-nav__btn">Login</a><!-- /.main-nav__btn -->
                    </div><!-- /.main-nav__right -->
                </div><!-- /.container-fluid -->
            </nav><!-- /.main-nav__one -->

        </header><!-- /.site-header-one -->


        <?= $this->renderSection('content'); ?>


        <footer class="site-footer-one site-footer-one__home-one">

            <div class="site-footer-one__upper">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-4">
                            <div class="footer-widget footer-widget__about">
                                <a href="index.html" class="footer-widget__logo">
                                    CeTas
                                </a>
                                <p>Lorem Ipsum is simply dummy text the printing and setting industry. Lorm Ipsum has been
                                    the
                                    text ever.</p>
                            </div><!-- /.footer-widget footer-widget__about -->
                        </div><!-- /.col-lg-4 -->
                        <div class="col-lg-3">
                            <div class="footer-widget footer-widget__links">
                                <h3 class="footer-widget__title">Explore</h3><!-- /.footer-widget__title -->
                                <div class="footer-widget__links-wrap">
                                    <ul class="list-unstyled footer-widget__links-list">
                                        <li><a href="about.html">About</a></li>
                                        <li><a href="prevention.html">Prevention</a></li>
                                        <li><a href="contact.html">Contact</a></li>
                                    </ul><!-- /.list-unstyled -->
                                    <ul class="list-unstyled footer-widget__links-list">
                                        <li><a href="faqs.html">FAQs</a></li>
                                        <li><a href="about.html">Privacy Policy</a></li>
                                        <li><a href="about.html">Terms of Use</a></li>
                                    </ul><!-- /.list-unstyled -->
                                </div><!-- /.footer-widget__links-wrap -->

                            </div><!-- /.footer-widget footer-widget__about -->
                        </div><!-- /.col-lg-3 -->
                        <div class="col-lg-3">
                            <div class="footer-widget footer-widget__contact">
                                <h3 class="footer-widget__title">Kontak</h3><!-- /.footer-widget__title -->
                                <p><a href="tel:888-999-0000">0000 0000 0000</a></p>
                                <p><a href="mailto:needhelp@vimns.com">bantuan@mail.com</a> </p>
                                <p>jalan x, kecamatan y <br> jakarta</p>
                            </div><!-- /.footer-widget footer-widget__contact -->
                        </div><!-- /.col-lg-3 -->
                        <div class="col-lg-2 d-flex">
                            <div class="footer-widget my-auto">
                                <div class="footer-widget__social">
                                    <a href="#" class="fab fa-facebook-square"></a>
                                    <a href="#" class="fab fa-twitter"></a>
                                    <a href="#" class="fab fa-instagram"></a>
                                    <a href="#" class="fab fa-pinterest-p"></a>
                                </div><!-- /.footer-widget__social -->
                            </div><!-- /.footer-widget -->
                        </div><!-- /.col-lg-2 -->
                    </div><!-- /.row -->
                </div><!-- /.container -->
            </div><!-- /.site-footer-one__upper -->
            <div class="site-footer-one__bottom">
                <div class="container">
                    <p>© copyright 2021</p>
                </div><!-- /.container -->
            </div><!-- /.site-footer-one__bottom -->
        </footer><!-- /.site-footer-one -->
    </div><!-- /.page-wrapper -->


    <a href="#" data-target="html" class="scroll-to-target scroll-to-top"><i class="fa fa-angle-up"></i></a>

    <script src="assets/js/jquery.min.js"></script>
    <script src="assets/js/bootstrap.bundle.min.js"></script>
    <script src="assets/js/owl.carousel.min.js"></script>
    <script src="assets/js/waypoints.min.js"></script>
    <script src="assets/js/jquery.counterup.min.js"></script>
    <script src="assets/js/TweenMax.min.js"></script>
    <script src="assets/js/wow.js"></script>
    <script src="assets/js/jquery.magnific-popup.min.js"></script>
    <script src="assets/js/jquery.ajaxchimp.min.js"></script>
    <script src="assets/js/jquery.validate.min.js"></script>
    <script src="assets/js/bootstrap-select.min.js"></script>
    <script src="assets/js/jquery.mCustomScrollbar.concat.min.js"></script>
    <script src="assets/js/bootstrap-datepicker.min.js"></script>
    <script src="assets/js/isotope.js"></script>

    <!-- template scripts -->
    <script src="assets/js/theme.js"></script>
    <script src="assets/js/testimonials-owl.carousel.config.js"></script>
</body>

</html>