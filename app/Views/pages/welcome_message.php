<?= $this->extend('layout/template1'); ?>

<?= $this->section('content'); ?>
        <section class="banner-one">
            <img src="assets/images/shapes/banner-bg-shape-1-1.png" alt="" class="banner-one__moc">
            <div class="container">
                <div class="banner-one__video wow fadeInUp" data-wow-animation-duration="1500ms">
                    <img src="assets/images/resources/video-1-1.jpg" alt="">
                    <a href="https://www.youtube.com/watch?v=Xj1nUFFVK1E" class="video-popup banner-one__video-btn"><i class="fa fa-play"></i></a>
                </div><!-- /.banner-one__video -->
                <div class="row">
                    <div class="col-lg-6">
                        <div class="banner-one__content">
                            <h3>Bagaimana Melindungi Diri Anda <br>
                                Dari <span>CoronaVirus?</span></h3>
                            <a href="/r/vaccine-registration.html" class="thm-btn banner-one__btn">Registrasi</a>
                            <!-- /.thm-btn banner-one__btn -->
                        </div><!-- /.banner-one__content -->
                    </div><!-- /.col-lg-6 -->
                </div><!-- /.row -->
            </div><!-- /.container -->
        </section><!-- /.banner-one -->

        <section class="cta-three">
            <img src="assets/images/shapes/about-bg-1-1.png" class="cta-three__bg" alt="">
            <div class="container">
                <div class="inner-container">
                    <div class="cta-three__icon">
                        <i class="vimns-icon-alert"></i>
                    </div><!-- /.cta-three__icon -->
                    <div class="cta-three__content">
                        <h3>Waspada penyakit Coronavirus COVID-19</h3>
                        <div class="cta-three__btn-block">
                            <a href="contact.html" class="cta-three__btn">Bantuan dan Informasi</a><!-- /.cta-three__btn -->
                        </div><!-- /.cta-three__btn-block -->
                    </div><!-- /.cta-three__content -->
                </div><!-- /.inner-container -->
            </div><!-- /.container -->
        </section><!-- /.cta-three -->

        <section class="about-one">
            <img src="assets/images/shapes/virus-shape-1-1.png" class="about-one__virus" alt="">
            <div class="container">
                <div class="inner-container">
                    <div class="row">
                        <div class="col-lg-6">
                            <div class="about-one__images wow fadeInLeft" data-wow-animation-duration="1500ms">
                                <img src="assets/images/resources/about-1-1.jpg" alt="">
                                <img src="assets/images/resources/about-1-2.jpg" alt="">
                                <a href="href=https://www.youtube.com/watch?v=Xj1nUFFVK1E" class="about-one__video-btn video-popup"><i class="fa fa-play"></i></a>
                                <!-- /.about-one__video-btn video-popup -->
                            </div><!-- /.about-one__images -->
                            <div class="about-one__fact-wrap">
                                <div class="about-one__fact">
                                    <div class="about-one__fact-icon">
                                        <i class="vimns-icon-mask"></i>
                                    </div><!-- /.about-one__fact-icon -->
                                    <div class="about-one__fact-content">
                                        <h4>94600000</h4>
                                        <p>Pasien <br> Pulih</p>
                                    </div><!-- /.about-one__fact-content -->
                                </div><!-- /.about-one__fact -->
                            </div><!-- /.about-one__fact-wrap -->
                        </div><!-- /.col-lg-6 -->
                        <div class="col-lg-6">
                            <div class="about-one__content">
                                <div class="block-title text-left">
                                    <p>Pengenalan Coronavirus </p>
                                    <h3>Penyakit Coronavirus (COVID-19)</h3>
                                </div><!-- /.block-title -->
                                <div class="about-one__icon-box">
                                    <div class="about-one__icon">
                                        <i class="vimns-icon-virus"></i>
                                    </div><!-- /.about-one__icon -->
                                    <div class="about-one__icon-content">
                                        <p>Penyakit Coronavirus merupakan penyakit infeksi yang disebabkan oleh virus baru.</p>
                                    </div><!-- /.about-one__icon-content -->
                                </div><!-- /.about-one__icon-box -->
                                <p>Langkah utama pencegahan COVID-19 adalah sebagai berikut:</p>

                                <ul class="list-unstyled about-one__list">
                                    <li><i class="vimns-icon-tick"></i> Cuci tangan Anda secara teratur selama 20 detik.</li>
                                    <li><i class="vimns-icon-tick"></i> Tutupi hidung dan mulut Anda dengan masker.</li>
                                    <li><i class="vimns-icon-tick"></i> Hindari kontak dekat (1 meter) dengan orang.
                                    </li>
                                    <li><i class="vimns-icon-tick"></i> Tetap di rumah dan isolasi diri dari orang lain jika terdapat gejala.
                                    </li>
                                    <li><i class="vimns-icon-tick"></i> Lindungi diri Anda dan bantu cegah penyebaran virus.
                                    </li>
                                </ul><!-- /.list-unstyled about-one__list -->
                                <a href="#" class="thm-btn about-one__btn">Pelajari Lebih Lanjut</a><!-- /.thm-btn about-one__btn -->
                            </div><!-- /.about-one__content -->
                        </div><!-- /.col-lg-6 -->
                    </div><!-- /.row -->
                </div><!-- /.inner-container -->
            </div><!-- /.container -->
        </section><!-- /.about-one -->

        <section class="funfact-one">
            <div class="container">
                <img src="assets/images/shapes/funfact-virus-1-1.png" class="funfact-one__virus-1" alt="">
                <img src="assets/images/shapes/funfact-virus-1-2.png" class="funfact-one__virus-2" alt="">
                <h3><span class="counter">156077,747</span></h3>
                <p>Orang yang terinfeksi COVID-19</p>
            </div><!-- /.container -->
        </section><!-- /.funfact-one -->

        <section class="service-one">
            <div class="container">
                <div class="block-title text-center">
                    <h3>Bagaimana Virus Menyebar</h3>
                </div><!-- /.block-title -->

                <div class="row no-gutters">
                    <div class="col-lg-4 wow fadeInLeft" data-wow-animation-duration="1500ms">
                        <div class="service-one__single">
                            <div class="service-one__inner">
                                <div class="service-one__image">
                                    <img src="assets/images/shapes/service-i-1.png" alt="">
                                </div><!-- /.service-one__image -->
                                <div class="service-one__content">
                                    <h3><a href="#">Kontak Dekat Dengan Orang yang Terinfeksi </a></h3>
                                    <p> Orang dengan COVID-19 dapat menularkan tetesan kecil air yang mengandung virus kepada orang lain saat batuk, bersin atau bernapas </p>
                                    <a href="#" class="service-one__link"><i class="vimns-icon-front"></i></a>
                                    <!-- /.service-one__link -->
                                </div><!-- /.service-one__content -->
                            </div><!-- /.service-one__inner -->

                        </div><!-- /.service-one__single -->
                    </div><!-- /.col-lg-4 -->
                    <div class="col-lg-4 wow fadeInUp" data-wow-animation-duration="1500ms">
                        <div class="service-one__single">
                            <div class="service-one__inner">
                                <div class="service-one__image">
                                    <img src="assets/images/shapes/service-i-2.png" alt="">
                                </div><!-- /.service-one__image -->
                                <div class="service-one__content">
                                    <h3><a href="#">Kontak Dekat Dengan Orang yang Terinfeksi </a></h3>
                                    <p> Orang dengan COVID-19 dapat menularkan tetesan kecil air yang mengandung virus kepada orang lain saat batuk, bersin atau bernapas </p>
                                    <a href="#" class="service-one__link"><i class="vimns-icon-front"></i></a>
                                    <!-- /.service-one__link -->
                                </div><!-- /.service-one__content -->
                            </div><!-- /.service-one__inner -->

                        </div><!-- /.service-one__single -->
                    </div><!-- /.col-lg-4 -->
                    <div class="col-lg-4 wow fadeInRight" data-wow-animation-duration="1500ms">
                        <div class="service-one__single">
                            <div class="service-one__inner">
                                <div class="service-one__image">
                                    <img src="assets/images/shapes/service-i-3.png" alt="">
                                </div><!-- /.service-one__image -->
                                <div class="service-one__content">
                                    <h3><a href="#">Kontak Dekat Dengan Orang yang Terinfeksi </a></h3>
                                    <p> Orang dengan COVID-19 dapat menularkan tetesan kecil air yang mengandung virus kepada orang lain saat batuk, bersin atau bernapas </p>
                                    <a href="#" class="service-one__link"><i class="vimns-icon-front"></i></a>
                                    <!-- /.service-one__link -->
                                </div><!-- /.service-one__content -->
                            </div><!-- /.service-one__inner -->

                        </div><!-- /.service-one__single -->
                    </div><!-- /.col-lg-4 -->
                </div><!-- /.row -->
            </div><!-- /.container -->
        </section><!-- /.service-one -->

        <section class="cta-two wow fadeInUp" data-wow-animation-duration="1500ms" style="background-image: url(assets/images/background/cta-bg-1-1.jpg);">
            <div class="container">
                <h3>Mari berpartisipasi<br>
                    melindungi sesama.</h3>
                <a href="/r/vaccine-registration.html" class="thm-btn cta-two__btn">Registrasi</a><!-- /.thm-btn cta-two__btn -->
            </div><!-- /.container -->
        </section><!-- /.cta-two -->
<?= $this->endSection(); ?>