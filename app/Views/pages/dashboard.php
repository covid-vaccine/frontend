  <?= $this->extend('layout/template2'); ?>

  <?= $this->section('content'); ?>
  <!-- start content -->
  <div class="content">
    <!-- start content head -->
    <div class="head">
      <!-- head top -->
      <div class="top">
        <div class="left">
          <button id="on" class="btn btn-info"><i class="fa fa-bars"></i></button>
          <button id="off" class="btn btn-info hide"><i class="fa fa-align-left"></i></button>
          <button class="btn btn-info hidden-xs-down"><i class="fa fa-expand-arrows-alt"></i></button>
          <a href="/"><button class="btn btn-info hidden-xs-down"><i class="fa fa-home"></i>Back Home</button></a>
        </div>
        <div class="right">
          <button class="btn btn-info hidden-xs-down"><i class="fa fa-bell"></i></button>
          <div class="dropdown">
            <button class="btn btn-info dropdown-toggle" id="userDropdown" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><?php echo $user['email'] ?></button>
            <div class="dropdown-menu" aria-labelledby="userDropdown">
             <a class="dropdown-item" href="#">profile</a>
             <a class="dropdown-item" href="#">setting</a>
             <a class="dropdown-item" href="/login/logout">log out</a>
           </div>
          </div>
        </div>
      </div>
      <!-- end head top -->
      <!-- start head bottom -->
      <div class="bottom">
        <div class="left">
          <h1>dashboard</h1>
        </div>
        <div class="right">
          <h1>dashboard /</h1>
          <a href="#">pengumuman</a>
        </div>
      </div>
      <!-- end head bottom -->
    </div>
    <!-- end content head -->
    <!-- start with the real content -->
    <div id="real">
      <!-- start content here -->
      <div class="wrap">
        <section class="app-content" id="inbox">
          <div class="row">
            <div class="col-md-10">
              <div class="row">
                <div class="col-md-12">
                  <div class="mail-toolbar m-b-lg">

                    <div class="btn-group" role="group">
                      <a href="#" class="btn btn-default"><i class="fa fa-trash"></i></a>
                      <a href="#" class="btn btn-default"><i class="fa fa-exclamation-circle"></i></a>
                    </div>

                    <div class="btn-group pull-right" role="group">
                      <a href="#" class="btn btn-default"><i class="fa fa-chevron-left"></i></a>
                      <a href="#" class="btn btn-default"><i class="fa fa-chevron-right"></i></a>
                    </div>
                  </div>
                </div>
              </div>

              <div class="table-responsive">
                <table class="table mail-list">
                  <tr>
                    <td>
                      <!-- a single mail -->
                      <div class="mail-item card">
                        <table class="mail-container">
                          <tr>
                            <td class="mail-left">
                              <div class="avatar avatar-lg avatar-circle">
                                <a href="#"><img src="assets3/img/correct.png" alt="sender photo"></a>
                              </div>
                            </td>
                            <td class="mail-center">
                              <div class="mail-item-header">
                                <h4 class="mail-item-title"><a href="/invoice" class="title-color">Program Vaksinasi Juni 2021</a></h4>
                                <a href="#"><span class="label label-success">siap vaksinasi</span></a>
                              </div>
                              <p class="mail-item-excerpt">Seluruh fasilitas kesehatan vaksinasi Covid-19</p>
                            </td>
                            <td class="mail-right">
                              <p class="mail-item-date">17/5/2021</p>
                              <p class="mail-item-star starred">
                                <a href="#"><i class="zmdi zmdi-star"></i></a>
                              </p>
                            </td>
                          </tr>
                        </table>
                      </div><!-- END mail-item -->
                      <!-- a single mail -->
                      <div class="mail-item card">
                        <table class="mail-container">
                          <tr>
                            <td class="mail-left">
                              <div class="avatar avatar-lg avatar-circle">
                                <a href="#"><img src="assets3/img/sad.png" alt="sender photo"></a>
                              </div>
                            </td>
                            <td class="mail-center">
                              <div class="mail-item-header">
                                <h4 class="mail-item-title"><a href="#" class="title-color">Program Vaksinasi Mei 2021</a></h4>
                                <a href="#"><span class="label label-success">belum siap vaksinasi</span></a>
                              </div>
                              <p class="mail-item-excerpt">Pendaftaran dibuka kembali pada tanggal 17 Mei 2021</p>
                            </td>
                            <td class="mail-right">
                              <p class="mail-item-date">15/5/2021</p>
                              <p class="mail-item-star starred">
                                <a href="#"><i class="zmdi zmdi-star"></i></a>
                              </p>
                            </td>
                          </tr>
                        </table>
                      </div><!-- END mail-item -->
                    </td>
                  </tr>
                </table>
              </div>
            </div><!-- END column -->
          </div><!-- .row -->
        </section><!-- .app-content -->
      </div><!-- .wrap -->

      <!-- Compose modal -->
      <div class="modal fade" id="composeModal" tabindex="-1" role="dialog">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
              <h4 class="modal-title">New Message</h4>
            </div>
            <div class="modal-body">
              <form action="#">
                <div class="form-group">
                  <input name="mail_from_field" id="mail_from_field" type="text" class="form-control" placeholder="from">
                </div>
                <div class="form-group">
                  <input name="mail_to_field" id="mail_to_field" type="text" class="form-control" placeholder="to">
                </div>
                <div class="form-group">
                  <input name="mail_subject_field" id="mail_subject_field" type="text" class="form-control" placeholder="subject">
                </div>
                <textarea name="mail_body_field" id="mail_body_field" cols="30" rows="5" class="form-control" placeholder="content"></textarea>
              </form>
            </div>
            <div class="modal-footer">
              <button type="button" data-dismiss="modal" class="btn btn-danger"><i class="fa fa-trash"></i></button>
              <button type="button" data-dismiss="modal" class="btn btn-success"><i class="fa fa-save"></i></button>
              <button type="button" data-dismiss="modal" class="btn btn-primary">Send <i class="fa fa-send"></i></button>
            </div>
          </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
      </div><!-- /.modal -->

      <!-- new label Modal -->
      <div id="labelModal" class="modal fade" tabindex="-1" role="dialog">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
              <h4 class="modal-title">Create / update label</h4>
            </div>
            <form action="#" id="newCategoryForm">
              <div class="modal-body">
                <div class="form-group m-0">
                  <input type="text" id="catLabel" class="form-control" placeholder="Label">
                </div>
              </div><!-- .modal-body -->
              <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
                <button type="button" class="btn btn-success" data-dismiss="modal">Save</button>
              </div><!-- .modal-footer -->
            </form>
          </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
      </div><!-- /.modal -->

      <!-- delete item Modal -->
      <div id="deleteItemModal" class="modal fade" tabindex="-1" role="dialog">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
              <h4 class="modal-title">Delete item</h4>
            </div>
            <div class="modal-body">
              <h5>Do you really want to delete this item ?</h5>
            </div><!-- .modal-body -->
            <div class="modal-footer">
              <button type="button" class="btn btn-danger" data-dismiss="modal">Delete</button>
            </div><!-- .modal-footer -->
          </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
      </div><!-- /.modal -->
      <!-- end content -->
    </div>
    <!-- end the real content -->
  </div>
  <!-- end content -->
  <?= $this->endSection(); ?>