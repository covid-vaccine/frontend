<?= $this->extend('layout/template1'); ?>

<?= $this->section('content'); ?>
<section class="page-header">
            <div class="container">
                <h2>Tentang Kami</h2>
                <ul class="thm-breadcrumb list-unstyled">
                    <li><a href="#">Beranda</a></li>
                    <li><span>Tentang kami</span></li>
                </ul><!-- /.thm-breadcrumb -->
            </div><!-- /.container -->
        </section><!-- /.page-header -->

        <section class="about-three">
            <div class="container">
                <div class="block-title text-center">
                    <p>More Know About Corona</p>
                    <h3>Protect Yourself From Cronoavirus</h3>
                </div><!-- /.block-title -->

                <div class="row">
                    <div class="col-lg-4">
                        <div class="about-three__image">
                            <img src="assets/images/resources/about-2-1.png" alt="">
                        </div><!-- /.about-three__image -->
                        <div class="about-three__title">
                            <h3>We’re Always Here to Protect You From Virus</h3>
                        </div><!-- /.about-three__title -->
                    </div><!-- /.col-lg-4 -->
                    <div class="col-lg-8">
                        <div class="about-three__image">
                            <img src="assets/images/resources/about-2-2.png" alt="">
                        </div><!-- /.about-three__image -->
                    </div><!-- /.col-lg-8 -->
                </div><!-- /.row -->
                <div class="text-center about-three__text">
                    <p>There are many variations of passages of available but the majority have suffered alteration in some <br>
                        form, by injected humou or randomised words. Proin ac lobortis arcu, a vestibulum aug ipsum neque, <br>
                        facilisis vel mollis vitae. Quisque aliquam dictum condim.</p>
                </div><!-- /.text-center -->
            </div><!-- /.container -->
        </section><!-- /.about-three -->

        <section class="cta-two" style="background-image: url(assets/images/background/cta-bg-1-1.jpg);">
            <div class="container">
                <h3>It’s Our Government Job to Project <br>
                    Themselves And Others</h3>
                <a href="contact.html" class="thm-btn cta-two__btn">Contact With Us</a><!-- /.thm-btn cta-two__btn -->
            </div><!-- /.container -->
        </section><!-- /.cta-two -->
<?= $this->endSection(); ?>