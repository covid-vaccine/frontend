<?= $this->extend('layout/template1'); ?>

<?= $this->section('content'); ?>
        <section class="page-header">
            <div class="container">
                <h2>Pencegahan</h2>
                <ul class="thm-breadcrumb list-unstyled">
                    <li><a href="#">Beranda</a></li>
                    <li><span>Pencegahan</span></li>
                </ul><!-- /.thm-breadcrumb -->
            </div><!-- /.container -->
        </section><!-- /.page-header -->

        <section class="prevention-one prevention-one__prevention-page">
            <div class="container">
                <div class="block-title text-center">
                    <h3>Tindakan Pencegahan COVID-19</h3>
                </div><!-- /.block-title text-center -->
                <div class="row">
                    <div class="col-lg-6">
                        <div class="prevention-one__box">
                            <div class="prevention-one__box-top">
                                <h3>Things You Should Do</h3>
                            </div><!-- /.prevention-one__box-top -->
                            <div class="prevention-one__box-bottom">
                                <div class="prevention-one__single">
                                    <div class="prevention-one__icon">
                                        <div class="prevention-one__icon-inner">
                                            <img src="assets/images/shapes/do1.jpg" alt="">
                                        </div><!-- /.prevention-one__icon-inner -->
                                    </div><!-- /.prevention-one__icon -->
                                    <div class="prevention-one__content">
                                        <h3>Wash Your Hands 20 Seconds </h3>
                                        <p>There are many of passages of lorem Ipsum but the available majority.</p>
                                    </div><!-- /.prevention-one__content -->
                                </div><!-- /.prevention-one__single -->

                                <div class="prevention-one__single">
                                    <div class="prevention-one__icon">
                                        <div class="prevention-one__icon-inner">
                                            <img src="assets/images/shapes/do2.jpg" alt="">
                                        </div><!-- /.prevention-one__icon-inner -->
                                    </div><!-- /.prevention-one__icon -->
                                    <div class="prevention-one__content">
                                        <h3>Wash Your Hands 20 Seconds </h3>
                                        <p>There are many of passages of lorem Ipsum but the available majority.</p>
                                    </div><!-- /.prevention-one__content -->
                                </div><!-- /.prevention-one__single -->

                                <div class="prevention-one__single">
                                    <div class="prevention-one__icon">
                                        <div class="prevention-one__icon-inner">
                                            <img src="assets/images/shapes/do3.jpg" alt="">
                                        </div><!-- /.prevention-one__icon-inner -->
                                    </div><!-- /.prevention-one__icon -->
                                    <div class="prevention-one__content">
                                        <h3>Wash Your Hands 20 Seconds </h3>
                                        <p>There are many of passages of lorem Ipsum but the available majority.</p>
                                    </div><!-- /.prevention-one__content -->
                                </div><!-- /.prevention-one__single -->
                                <div class="prevention-one__single">
                                    <div class="prevention-one__icon">
                                        <div class="prevention-one__icon-inner">
                                            <img src="assets/images/shapes/do4.jpg" alt="">
                                        </div><!-- /.prevention-one__icon-inner -->
                                    </div><!-- /.prevention-one__icon -->
                                    <div class="prevention-one__content">
                                        <h3>Wash Your Hands 20 Seconds </h3>
                                        <p>There are many of passages of lorem Ipsum but the available majority.</p>
                                    </div><!-- /.prevention-one__content -->
                                </div><!-- /.prevention-one__single -->
                            </div><!-- /.prevention-one__box-bottom -->
                        </div><!-- /.prevention-one__box -->
                    </div><!-- /.col-lg-6 -->
                    <div class="col-lg-6">
                        <div class="prevention-one__box">
                            <div class="prevention-one__box-top">
                                <h3>Things You Shouldn't Do</h3>
                            </div><!-- /.prevention-one__box-top -->
                            <div class="prevention-one__box-bottom">
                                <div class="prevention-one__single">
                                    <div class="prevention-one__icon">
                                        <div class="prevention-one__icon-inner">
                                            <img src="assets/images/shapes/dont1.jpg" alt="">
                                        </div><!-- /.prevention-one__icon-inner -->
                                    </div><!-- /.prevention-one__icon -->
                                    <div class="prevention-one__content">
                                        <h3>Wash Your Hands 20 Seconds </h3>
                                        <p>There are many of passages of lorem Ipsum but the available majority.</p>
                                    </div><!-- /.prevention-one__content -->
                                </div><!-- /.prevention-one__single -->

                                <div class="prevention-one__single">
                                    <div class="prevention-one__icon">
                                        <div class="prevention-one__icon-inner">
                                            <img src="assets/images/shapes/dont2.jpg" alt="">
                                        </div><!-- /.prevention-one__icon-inner -->
                                    </div><!-- /.prevention-one__icon -->
                                    <div class="prevention-one__content">
                                        <h3>Wash Your Hands 20 Seconds </h3>
                                        <p>There are many of passages of lorem Ipsum but the available majority.</p>
                                    </div><!-- /.prevention-one__content -->
                                </div><!-- /.prevention-one__single -->

                                <div class="prevention-one__single">
                                    <div class="prevention-one__icon">
                                        <div class="prevention-one__icon-inner">
                                            <img src="assets/images/shapes/dont3.jpg" alt="">
                                        </div><!-- /.prevention-one__icon-inner -->
                                    </div><!-- /.prevention-one__icon -->
                                    <div class="prevention-one__content">
                                        <h3>Wash Your Hands 20 Seconds </h3>
                                        <p>There are many of passages of lorem Ipsum but the available majority.</p>
                                    </div><!-- /.prevention-one__content -->
                                </div><!-- /.prevention-one__single -->
                                <div class="prevention-one__single">
                                    <div class="prevention-one__icon">
                                        <div class="prevention-one__icon-inner">
                                            <img src="assets/images/shapes/dont4.jpg" alt="">
                                        </div><!-- /.prevention-one__icon-inner -->
                                    </div><!-- /.prevention-one__icon -->
                                    <div class="prevention-one__content">
                                        <h3>Wash Your Hands 20 Seconds </h3>
                                        <p>There are many of passages of lorem Ipsum but the available majority.</p>
                                    </div><!-- /.prevention-one__content -->
                                </div><!-- /.prevention-one__single -->
                            </div><!-- /.prevention-one__box-bottom -->
                        </div><!-- /.prevention-one__box -->
                    </div><!-- /.col-lg-6 -->
                </div><!-- /.row -->
            </div><!-- /.container -->
        </section><!-- /.prevention-one -->

        <section class="hand-wash-one">
            <div class="container">
                <div class="block-title text-center">
                    <p>6 Langkah Mudah</p>
                    <h3>Prosedur Cuci Tangan</h3>
                </div><!-- /.block-title text-center -->
                <div class="row">
                    <div class="col-lg-3 col-md-6 wow fadeInUp" data-wow-duration="1500ms" data-wow-delay="000ms">
                        <div class="hand-wash-one__single">
                            <div class="hand-wash-one__image">
                                <img src="assets/images/shapes/wash1.png" alt="">
                            </div><!-- /.hand-wash-one__image -->
                            <h3>Basahi tangan dengan air</h3><br/><br/>
                        </div><!-- /.hand-wash-one__single -->
                    </div><!-- /.col-lg-3 col-md-6 wow fadeInUp -->
                    <div class="col-lg-3 col-md-6 wow fadeInUp" data-wow-duration="1500ms" data-wow-delay="100ms">
                        <div class="hand-wash-one__single">
                            <div class="hand-wash-one__image">
                                <img src="assets/images/shapes/wash2.png" alt="">
                            </div><!-- /.hand-wash-one__image -->
                            <h3>Gunakan sabun secukupnya untuk menjangkau seluruh permukaan tangan</h3>
                        </div><!-- /.hand-wash-one__single -->
                    </div><!-- /.col-lg-3 col-md-6 wow fadeInUp -->
                    <div class="col-lg-3 col-md-6 wow fadeInUp" data-wow-duration="1500ms" data-wow-delay="200ms">
                        <div class="hand-wash-one__single">
                            <div class="hand-wash-one__image">
                                <img src="assets/images/shapes/wash3.png" alt="">
                            </div><!-- /.hand-wash-one__image -->
                            <h3>Gosok seluruh permukaan tangan</h3><br/>
                        </div><!-- /.hand-wash-one__single -->
                    </div><!-- /.col-lg-3 col-md-6 wow fadeInUp -->
                    <div class="col-lg-3 col-md-6 wow fadeInUp" data-wow-duration="1500ms" data-wow-delay="300ms">
                        <div class="hand-wash-one__single">
                            <div class="hand-wash-one__image">
                                <img src="assets/images/shapes/wash4.png" alt="">
                            </div><!-- /.hand-wash-one__image -->
                            <h3>Cuci bagian depan dan belakang tangan, serta selah-selah jari</h3>
                        </div><!-- /.hand-wash-one__single -->
                    </div><!-- /.col-lg-3 col-md-6 wow fadeInUp -->
                    <div class="col-lg-3 col-md-6 wow fadeInUp" data-wow-duration="1500ms" data-wow-delay="300ms">
                        <div class="hand-wash-one__single">
                            <div class="hand-wash-one__image">
                                <img src="assets/images/shapes/wash5.png" alt="">
                            </div><!-- /.hand-wash-one__image -->
                            <h3>Bilas dengan air</h3><br/><br/>
                        </div><!-- /.hand-wash-one__single -->
                    </div><!-- /.col-lg-3 col-md-6 wow fadeInUp -->
                    <div class="col-lg-3 col-md-6 wow fadeInUp" data-wow-duration="1500ms" data-wow-delay="300ms">
                        <div class="hand-wash-one__single">
                            <div class="hand-wash-one__image">
                                <img src="assets/images/shapes/wash6.png" alt="">
                            </div><!-- /.hand-wash-one__image -->
                            <h3>Keringkan tangan Anda</h3><br/><br/>
                        </div><!-- /.hand-wash-one__single -->
                    </div><!-- /.col-lg-3 col-md-6 wow fadeInUp -->
                </div><!-- /.row -->
            </div><!-- /.container -->
        </section><!-- /.hand-wash-one -->


        <section class="cta-one">
            <div class="container">
                <div class="cta-one__left">
                    <h3>How You'll Protect Yourself and Stay Safe</h3>
                </div><!-- /.cta-one__left -->
                <div class="cta-one__right">
                    <a href="#" class="thm-btn cta-one__btn">Discover More</a><!-- /.thm-btn cta-one__btn -->
                </div><!-- /.cta-one__right -->
            </div><!-- /.container -->
        </section><!-- /.cta-one -->

<?= $this->endSection(); ?>