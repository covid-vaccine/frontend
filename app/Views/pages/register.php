<!DOCTYPE html>
<html lang="en">

  <head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <!-- start linking  -->
    <link href="https://fonts.googleapis.com/css?family=Nunito:300,400,600,700,800,900" rel="stylesheet">
    <link href="https://use.fontawesome.com/releases/v5.0.4/css/all.css" rel="stylesheet">
    <link rel="stylesheet" href="assets3/css/bootstrap.min.css">
    <link rel="stylesheet" href="assets3/css/app.css">
    <!-- icon -->
    <link rel="icon" href="assets3/img/log.png">
    <!-- end linking -->
    <title>NutFlix - admin</title>
  </head>
  <body>
      <div class="content">
        <div id="real">
          <div id="forms">
            <div class="wrap card">
              <div class=" pt-32pt pt-sm-64pt pb-32pt">
                <div class="container-fluid page__container">
                    <div class="col-md-5 p-0 mx-auto">
                      <h3>Pendaftaran Akun</h3>
                    </div>
                    <form action="/register/save"
                          method="post"
                          class="col-md-5 p-0 mx-auto">
                        <div class="form-group">
                            <label class="form-label"
                                   for="name1">Nama Depan:</label>
                            <input id="name1"
                                   name="name1"
                                   type="text"
                                   class="form-control">
                        </div>
                        <div class="form-group">
                            <label class="form-label"
                                   for="name2">Nama Belakang:</label>
                            <input id="name2"
                                   name="name2"
                                   type="text"
                                   class="form-control">
                        </div>
                        <div class="form-group">
                            <label class="form-label"
                                   for="email">Email:</label>
                            <input id="email"
                                   name="email"
                                   type="text"
                                   class="form-control"
                                   placeholder="example@email.com">
                        </div>
                        <div class="form-group">
                            <label class="form-label"
                                   for="password">Password:</label>
                            <input id="password"
                                   name="password"
                                   type="password"
                                   class="form-control"
                                   placeholder=" ********">
                        </div>
                        <div class="text-center">
                            <button type="submit" class="btn btn-primary">Register</button>
                        </div>
                    </form>
                </div>
             </div>
            </div>
          </div>
        </div>
      </div>
  </body>
</html>