<?= $this->extend('/layout/template1'); ?>

<?= $this->section('content'); ?>


        <section class="page-header">
            <div class="container">
                <h2>FAQs</h2>
                <ul class="thm-breadcrumb list-unstyled">
                    <li><a href="#">Beranda</a></li>
                    <li><span>FAQs</span></li>
                </ul><!-- /.thm-breadcrumb -->
            </div><!-- /.container -->
        </section><!-- /.page-header -->

        <section class="faq-one faq-one__faq-page">
            <div class="container">
                <div class="block-title text-center">
                    <p>Frequently Asked Questions </p>
                    <h3>Have any question?</h3>
                </div><!-- /.block-title -->

                <div class="row">
                    <div class="col-lg-6">
                        <div class="accrodion-grp" data-grp-name="faq-one-accrodion">
                            <div class="accrodion active">
                                <div class="accrodion-title">
                                    <h4>Apa yang harus saya lakukan jika saya memiliki kontak dekat dengan seseorang yang memiliki COVID-19?</h4>
                                </div>
                                <div class="accrodion-content">
                                    <div class="inner">
                                        <p>Cara terbaik untuk melindungi diri sendiri dan orang lain adalah dengan karantina dengan tinggal di 
                                            rumah selama 14 hari jika Anda merasa telah terpapar dengan seseorang yang memiliki COVID-19.
                                             Periksa situs web departemen kesehatan setempat untuk informasi tentang opsi di wilayah Anda 
                                             untuk kemungkinan mempersingkat masa karantina ini. </p>
                                    </div><!-- /.inner -->
                                </div>
                            </div>
                            <div class="accrodion ">
                                <div class="accrodion-title">
                                    <h4>Apa saja gejala dan komplikasi yang dapat ditimbulkan oleh COVID-19?</h4>
                                </div>
                                <div class="accrodion-content">
                                    <div class="inner">
                                        <p>Orang dengan COVID-19 telah melaporkan berbagai gejala – dari gejala ringan hingga penyakit parah.
                                             Gejala dapat muncul 2-14 hari setelah terpapar virus.
                                              Jika Anda mengalami demam, batuk, atau gejala lain, Anda mungkin terinfeksi COVID-19. </p>
                                    </div><!-- /.inner -->
                                </div>
                            </div>
                            <div class="accrodion ">
                                <div class="accrodion-title">
                                    <h4>Bisakah saya terpapar COVID-19 dari hewan peliharaan saya atau hewan lain?</h4>
                                </div>
                                <div class="accrodion-content">
                                    <div class="inner">
                                        <p>Berdasarkan informasi yang tersedia hingga saat ini, risiko hewan menyebarkan COVID-19 ke manusia dianggap rendah. </p>
                                    </div><!-- /.inner -->
                                </div>
                            </div>
                            <div class="accrodion ">
                                <div class="accrodion-title">
                                    <h4>Bisakah COVID-19 menular melalui nyamuk atau kutu ?</h4>
                                </div>
                                <div class="accrodion-content">
                                    <div class="inner">
                                        <p>Saat ini, tidak ada data yang menunjukkan bahwa coronavirus baru ini atau coronavirus serupa lainnya disebarkan oleh nyamuk atau kutu.
                                             Cara utama penyebaran COVID-19 adalah dari orang ke orang. </p>
                                    </div><!-- /.inner -->
                                </div>
                            </div>

                        </div>
                    </div><!-- /.col-lg-6 -->
                    <div class="col-lg-6">
                        <div class="accrodion-grp" data-grp-name="faq-two-accrodion">
                            <div class="accrodion ">
                                <div class="accrodion-title">
                                    <h4>Apakah pemberian vaksin aman untuk semua umur ?</h4>
                                </div>
                                <div class="accrodion-content">
                                    <div class="inner">
                                        <p>Iya. Studi menunjukkan bahwa vaksin COVID-19 aman dan efektif. Seperti orang dewasa, anak-anak mungkin memiliki beberapa efek samping setelah vaksinasi COVID-19.
                                             Efek samping ini dapat mempengaruhi kemampuan mereka untuk melakukan aktivitas sehari-hari, tetapi efek tersebut akan hilang dalam beberapa hari. </p>
                                    </div><!-- /.inner -->
                                </div>
                            </div>
                            <div class="accrodion active">
                                <div class="accrodion-title">
                                    <h4>Apa saja efek samping yang umum terjadi setelah melakukan vaksinasi ?</h4>
                                </div>
                                <div class="accrodion-content">
                                    <div class="inner">
                                        <p>Setelah divaksinasi, Anda mungkin mengalami beberapa efek samping, yang merupakan tanda normal bahwa tubuh Anda sedang membangun perlindungan. Efek samping yang umum adalah nyeri, 
                                            kemerahan, dan pembengkakan di lengan tempat Anda menerima suntikan, serta kelelahan, sakit kepala, nyeri otot, kedinginan, demam, dan mual di seluruh tubuh. </p>
                                    </div><!-- /.inner -->
                                </div>
                            </div>
                            <div class="accrodion ">
                                <div class="accrodion-title">
                                    <h4>Berapa lama vaksin dapat mencegah penularan COVID-19?</h4>
                                </div>
                                <div class="accrodion-content">
                                    <div class="inner">
                                        <p>Kami tidak tahu berapa lama perlindungan berlangsung bagi mereka yang divaksinasi.
                                             Apa yang kita ketahui adalah bahwa COVID-19 telah menyebabkan penyakit dan kematian yang sangat serius bagi banyak orang.
                                              Jika Anda terkena COVID-19, Anda juga berisiko memberikannya kepada orang-orang terkasih yang mungkin sakit parah.
                                               Mendapatkan vaksin COVID-19 adalah pilihan yang lebih aman. </p>
                                    </div><!-- /.inner -->
                                </div>
                            </div>
                            <div class="accrodion ">
                                <div class="accrodion-title">
                                    <h4>Bisakah saya mendapatkan Vaksin ketika saya sedang terpapar COVID-19 ?</h4>
                                </div>
                                <div class="accrodion-content">
                                    <div class="inner">
                                        <p>Tidak. Orang dengan COVID-19 yang memiliki gejala harus menunggu untuk divaksinasi sampai sembuh dari penyakitnya dan memenuhi kriteria untuk tidak melanjutkan isolasi, 
                                            mereka yang tidak memiliki gejala juga harus menunggu sampai mereka memenuhi kriteria sebelum divaksinasi. </p>
                                    </div><!-- /.inner -->
                                </div>
                            </div>

                        </div>
                    </div><!-- /.col-lg-6 -->

                </div><!-- /.row -->
            </div><!-- /.container -->
        </section><!-- /.faq-one -->

<?= $this->endSection(); ?>